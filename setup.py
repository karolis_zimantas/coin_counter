import setuptools

setuptools.setup(
    name='coin_count',
    version='0.0.0',
    description='Application prints a change to be returned from a given sum amount in the number of coins',
    py_modules=['coin_count'])
